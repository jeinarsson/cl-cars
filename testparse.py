from bs4 import BeautifulSoup
import requests
from requests.exceptions import RequestException

with open('car.html', 'r') as content_file:
    content = content_file.read()
soup = BeautifulSoup(content, 'html.parser')
attrgroups = soup.find_all('p', {'class','attrgroup'})
attrs = dict()
for group in attrgroups:
	spans = group.find_all('span')
	for span in spans:
		if not ':' in span.text:
			key = 'year'
			try:
				val = int(span.text[0:4])
			except ValueError:
				val = None
		else:
			key = span.text.split(':')[0]
			val = span.find('b').text
			
			if key == 'odometer':
				try:
					key = 'miles'
					val = int(val)
				except ValueError:
					val = None				

		attrs[key] = val

print (attrs)