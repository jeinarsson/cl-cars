import numpy as np
import matplotlib.pyplot as plt

from db import *

q=db_session.query(Car).filter(Car.model=='odyssey').filter(Car.year>2005).all()

data = {}
for r in q:
	if r.miles is None or r.price is None or r.year is None:
		print(r.miles)
		print(r.price)
	else:

		x = r.miles
		y = r.price
		key = str(r.year)
		if not key in data:
			data[key] = {'x':[],'y':[]}

		data[key]['x'].append(x)
		data[key]['y'].append(y)

from itertools import cycle
filled_markers = ('o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd')
markers = cycle(filled_markers)
for y in sorted(data.keys()):
	plt.plot(data[y]['x'],data[y]['y'],ls='',marker=next(markers),label=y)
plt.xlim(0,200000)
plt.ylim(0,25000)
plt.xlabel('Miles')
plt.ylabel('Dollars')
plt.title('Honda Odyssey')
plt.legend()
plt.savefig('odyssey.png')
plt.show()
