from craigslist import CraigslistForSale

from db import *


model = 'odyssey'

cl = CraigslistForSale(site='sfbay', category='cto',filters={
	#'posted_today': True,
	'min_price': 1000,
	'model': model,
	'query': 'leather'
	})

for result in cl.get_results(sort_by='newest', get_all_info=True):

	cl_id = result['id']
	try:
		c = db_session.query(Car).filter(Car.cl_id==cl_id).one()
		print('refound {}'.format(cl_id))
	except:
		c = Car(cl_id = cl_id)

	c.title = result['name']
	c.url = result['url']
	c.make = ''
	c.model = model
	c.miles = result['miles']
	c.year = result['year']
	c.price = result['price']

	c.latlong = result['latlong'] if 'latlong' in result else ''
		
	db_session.add(c)


db_session.commit()