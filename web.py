from flask import Flask,json,render_template
from db import *
app = Flask(__name__)


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

@app.route('/')
def hello_world():
    
    q=db_session.query(Car).filter(Car.model=='odyssey').filter(Car.year>2005).all()


    data = []
    for c in q:
        data.append({
            'title': c.title,
            'url': c.url,
            'make': c.make,
            'model': c.model,
            'miles': c.miles,
            'year': c.year,
            'price': c.price,
            'latlong': c.latlong
            })


    return render_template('car-scatter.html', data=data)

