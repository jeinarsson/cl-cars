from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///cars.db', echo=False)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


from sqlalchemy import Column, Integer, String

class Car(Base):
	__tablename__ = 'cars'
	id = Column(Integer, primary_key=True)
	cl_id = Column(String)
	title = Column(String)
	url = Column(String)
	make = Column(String)
	model = Column(String)
	miles = Column(Integer)
	year = Column(Integer)
	price = Column(Integer)
	latlong = Column(String)

